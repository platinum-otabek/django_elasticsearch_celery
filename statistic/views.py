from django.shortcuts import render
from .tasks  import async_write_history


# Create your views here.

def index(request):
    async_write_history('statistic/index')
    return render(request, 'index.html')


def users(request):
    async_write_history('statistic/users')
    return render(request, 'users.html')


def user_detail(request):
    async_write_history('statistic/user-detail')
    return render(request, 'user_detail.html')


def info(request):
    async_write_history('statistic/info')
    return render(request, 'info.html')
