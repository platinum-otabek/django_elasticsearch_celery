from celery import shared_task
from .utils import index_page_access


@shared_task()
def async_write_history(url):
    index_page_access(url)
