from elasticsearch_dsl import Document, Keyword, Date, connections
from datetime import datetime
import os
connections.create_connection(hosts=[f'http://{os.environ.get("PROJECT_NAME")}_elasticsearch:9200'])


class PageAccess(Document):
    url = Keyword()
    timestamp = Date()

    class Index:
        name = 'page_access'


def index_page_access(url):
    PageAccess(url=url, timestamp=datetime.utcnow()).save()
