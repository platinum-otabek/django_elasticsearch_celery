from django.urls import path
from .views import index, users, user_detail, info

urlpatterns = [
    path('', index),
    path('users', users),
    path('user-detail', user_detail),
    path('info', info),

]
