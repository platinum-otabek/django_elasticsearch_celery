FROM python:3.10-alpine as builder

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app

RUN python -m venv  /opt/venv

ENV PATH="/opt/venv/bin:$PATH"
COPY ./requirements.txt ./requirements.txt

RUN pip install --no-cache-dir -r requirements.txt
RUN pip install uvicorn

FROM python:3.10
WORKDIR /app

RUN apt update && apt install -y libpq-dev gettext

ENV PATH="/opt/venv/bin:$PATH"

COPY --from=builder /opt/venv /opt/venv
COPY . .
EXPOSE 8001

#CMD
#python manage.py collectstatic --noinput && \
#    python manage.py migrate && \
#    uvicorn config.asgi:application --host 0.0.0.0 --port 8003
