#!/bin/bash

# Start uvicorn in the background
#uvicorn config.asgi:application --host 0.0.0.0 --port 8003 &
python manage.py collectstatic --noinput && \
    python manage.py migrate && \
    uvicorn config.asgi:application --host 0.0.0.0 --port 8001

# Sleep for a short duration to allow uvicorn to start
#sleep 5

# Start Celery worker in the background
#celery -A config.celery worker -l info &
